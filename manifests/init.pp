class matrix (
  String $servername,
  Stdlib::Port $listen_port = 8008,
  Boolean $registrastration_enabled = false,
  String $package_name = 'matrix-synapse-py3',
  String $service_name = 'matrix-synapse',
) {

  require apt
  apt::source { 'synapse':
    locatino => 'https://packages.matrix.org/debian/',
    release  => $facts['os']['distro']['codename'],
    repos    => 'main',
    key      => 'AAF9AE843A7584B5A3E4CD2BCF45A512DE2DA058',
    include  => {
      'deb'  => true,
    },
  }

  package { $package_name:
    ensure => installed,
    alias  => 'matrix-synapse',
  }

  service { $service_name:
    ensure => running,
    enable => true,
  }


  include matrix::config
  include ::postgresql::server

  postgres::server::database { $db_name:
    encoding => 'UTF-8',
    locale   => 'C',
    owner    => $db_user,
    # before => Class['synapse']
  }

  postgresql::server::role { $db_user:
    update_password => true,
    login           => true,
    password_hash   => postgresql::postgresql_password(
      $db_user, $db_pass, false
    ),
  }
}
