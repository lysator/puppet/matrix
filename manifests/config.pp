class matrix::config (
) {

  file { [
    '/etc/matrix-synapse',
    '/etc/matrix-synapse/conf.d',
  ]:
    ensure => directory,
  }

  file { $log_conf_file:
    content        => hash2yaml({
      'version'    => 1,
      'formatters' => {
        'verbose' => {
          format => '%(module)s %(message)s'
        },
        'precise' => {
          'format' => '%(asctime)s - %(name)s - %(lineno)d - %(levelname)s - %(request)s - %(message)s',
        }
      },
      'handlers'   => {
        'sys-logger6' => {
          'class'     => 'logging.handlers.SysLogHandler',
          'address'   => '/dev/log',
          'facility'  => 'local6',
          'formatter' => 'verbose',
        },
        'console'     => {
          'class'     => 'logging.StreamHandler',
          'formatter' => 'precise',
        }
      },
      'loggers'    => {
        '.'  => {
          'handlers'  => [
            'sys-logger6',
            'console',
          ],
          'level'     => 'DEBUG',
          'propagate' => true,
        }
      },
    }, {
      'header' => '# This file is controlled by puppet',
    })
  }
  file { $log_conf_file:
    content        => hash2yaml({
      'version'    => 1,
      'formatters' => {
        'verbose' => {
          format => '%(module)s %(message)s'
        },
        'precise' => {
          'format' => '%(asctime)s - %(name)s - %(lineno)d - %(levelname)s - %(request)s - %(message)s',
        }
      },
      'handlers'   => {
        'sys-logger6' => {
          'class'     => 'logging.handlers.SysLogHandler',
          'address'   => '/dev/log',
          'facility'  => 'local6',
          'formatter' => 'verbose',
        },
        'console'     => {
          'class'     => 'logging.StreamHandler',
          'formatter' => 'precise',
        }
      },
      'loggers'    => {
        '.'  => {
          'handlers'  => [
            'sys-logger6',
            'console',
          ],
          'level'     => 'DEBUG',
          'propagate' => true,
        }
      },
    }, {
      'header' => '# This file is controlled by puppet',
    })
  }

  {
    'trusted_key_servers' => [
      {
        'server_name' => 'matrix.org',
      },
    ],
    'supress_key_server_warning' => true,
    'password_providers'         => [
    ],
    'registrations_require_3pid' => [
      'email',
    ],
    'email'        => {
      'notif_from' => '%(app)s@matrix.lysator.liu.se',
      'enable_tls' => false,
      'subjects'   => $email_subjects,
    },
    'app_service_config_file' => [
      '/home/lysroot/matrix-appservice-irc/appservice-registration-irc.yaml',
    ],
    'log_config' => [
      '/etc/matrix-synapse/log.yaml',
    ],
    'oidc_providers' => [
      $matrix::oidc::keycloak::config,
    ],
  }
}
