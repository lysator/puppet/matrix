define matrix::oidc::keycloak (
  String $description,
  String $issuer,
  String $client_id,
  Variant[String, Sensitive[String]] $client_secret,
  Boolean $backchannel_logout = true,
  Struct[{'localpart_template' => String, 'display_name_template' => String}] $user_mapping = {
    'localpart_template'    => '{{user.preferred_username}}',
    'display_name_template' => '{{user.name}}',
  },
) {
  $client_secret_ = $client_secret ? {
    String => $client_secret,
    _      => $client_secret.unwrap,
  }

  $config = {
    'idp_id'                     => 'keycloak',
    'idp_name'                   => $description,
    'issuer'                     => $issuer,
    'client_id'                  => $client_id,
    'client_secret'              => $client_secret_,
    'scopes'                     => ['openid', 'profile'],
    'backchannel_logout_enabled' => $backchannel_logout,
    'user_mapping_provider' => {
      'config' => $user_mapping,
    }
  }
}
