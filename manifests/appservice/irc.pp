# https://matrix-org.github.io/matrix-appservice-irc/latest/usage
# https://github.com/matrix-org/matrix-appservice-irc/blob/develop/config.sample.yaml
define matrix::appservice::irc (
  String $server_name = $name,
  String $domain,
  Stdlib::Port $port = 6697,
  Hash[String, Hash] $mappings = {},
  String $network_id,
  Boolean $ssl = true,
  Boolean $ssl_self_sign = false,
  Boolean $sasl = true,
  Boolean $allow_expired_certs = false,
  Boolean $send_connection_messages = true,
  Hash $quit_debounce = {},
  Struct[{'o' => Integer, 'v' => Integer}] $move_power_map = {
    'o' => 50, 'v' => 1,
  },
  Hash $bot_config = {
    'enabled' => false,
  },
  Hash $private_messages = {
    'enabled'  => true,
    'federate' => true,
  },
  Hash $dynamic_channels = {
    'enabled' => false,
  },
  Hash $membership_lists = {
    'enabled' => false,
  },
  Hash $matrix_clients = {
    'userTemplate' => '@$NICK[irc]',
    'displayName'  => '$NICK',
    'joinAttempts' => -1,
  },
  Hash $irc_clients = {
    'nickTemplate'     => '$DISPLAY[m]',
    'allowNickChanges' => true,
    'maxClients'       => 30,
  },
  Hash $ipv6 = {
    'only' => false,
  },
  Integer $line_limit = 15,
  Enum['mixd', 'reverse-mixd'] $realname_format = 'mixd',
  Hash $kick_on = {
    'channelJoinFailure'   => true,
    'ircConnectionFailure' => true,
    'userQuit'             => true,
  },


) {
  include matrix::appservice::irc::setup
  $service_file = "/etc/systemd/system/${service_name}"
  # NOT configurable, hard coded in service file
  $service_env_file = '/etc/default/matrix-appservice-irc'

  file { $matrix_appservice_irc_path:
    ensure => directory,
  }

  $debug_api = $irc_bridge_debug_port ? {
    Undef  => {},
    String => { 'debugApi' => {
      'enabled' => false,
      'port'    => $irc_bridge_debug_port,
    }},
  }

  $libera_settings = {
    'name'                   => $server_name,
    'networkId'              => $network_id,
    'port'                   => $port,
    'ssl'                    => $ssl,
    'sslselfsign'            => $ssl_self_sign,
    'sasl'                   => $sasl,
    'allowExpiredCerts'      => $allow_expired_certs,
    'sendConnectionMessages' => $send_connection_messages,
    'quitDebounce'           => $quit_debounce,
    'modePowerMap'           => $mode_power_map,
    'botConfig'              => $bot_config,
    'privateMessages'        => $private_messages,
    'dynamicChannels'        => $dynamic_channels,
    'membershipLists'        => $membership_lists,
    'mappings'               => $mappings,
    'matrixClients'          => $matrix_clients,
    'ircClients'             => $irc_clients,
    'ipv6'                   => $ipv6,
    'lineLimit'              => $line_limit,
    'realnameFormat'         => $realname_format,
    'kickOn'                 => $kick_on,
  }

  $hash = {
    $domain => $libera_settings,
  }

  $data = hash2yaml($hash, {'indentation' => 2})
  .split("\n")
  .map |$line| { "  ${line}" }
  .join("\n")

  concat::fragment { '':
    content => $data,
    target  => $matrix::appservice::irc::setup::config_file,
  }
}
