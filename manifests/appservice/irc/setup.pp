class matrix::appservice::irc::setup (
  String $homeserver_url,
  Stdlib::Port $bind_port = 8090,
  String $db_name = 'irc_bridge',
  String $db_user,
  String $db_host = 'localhost',
  Variant[String, Sensitive[String]] $db_pass,
  Boolean $manage_db_user = true,
  Optional[Stdlib::Port] $db_port = undef,
  Variant[String, Sensitive[String]] $passkey,
  String $config_file = '/etc/matrix-synapse/appservice-irc.yaml',
  String $service_name = 'matrix-appservice-irc.service',
  String $matrix_appservice_irc_path = '/home/lysroot/matrix-appservice-irc',
  String $registration_file = "${matrix_appservice_irc_path}/appservice-registration-irc.yaml",
  String $config_file = "${matrix_appservice_irc_path}/config.yaml",
  String $user_id = 'my_bot',
  Stdlib::Port $listen_port = 9999,
) {

  # TODO fetch code

  postgresql::server::database { $db_name:
    encoding => 'UTF-8',
    locale   => 'C',
    owner    => $db_user,
  }

  if $manage_db_user {
    postgresql::server::role { $db_user:
      update_password => true,
      login           => true,
      password_hash   => postgresql::postgresql_password(
        $db_user, $db_pass, false
      ),
    }
  }

  $db_pass_ = $db_pass ? {
    String => $db_pass,
    _      => $db_pass.unwrap,
  }

  $passkey = 'passkey.pem'
  exec { 'Generate password encryption key':
    command => ['openssl', 'genpkey',
                '-out', $passkey,
                '-outform', 'PEM',
                '-algorithm', 'RSA',
                '-pkeyeopt', 'rsa_keygen_bits:2048'],
    creates => "${matrix_appservice_irc_path}/${passkey}",
    path    => ['/usr/bin', '/bin'],
  }

  concat { $config_file:
    notify => [
      Service[$synapse::service_name],
      Service[$service_name],
    ],
  }

  concat::fragment {
    content => epp("${module_name}/appservice_irc.yaml.epp")
  }

  exec { 'Generate appservice file':
    command     => [
      'node', 'app.js',
      '--generate-registration',
      '--file', $registration_file,
      '--url', "http://localhost:${listen_port}",
      '--config', $config_file,
      '--localpart', $user_id,
    ],
    cwd         => $matrix_appservice_irc_path,
    # creates     => $registration_file,
    path        => ['/bin','/usr/bin'],
    subscribe   => File[$config_file],
    refreshonly => true,
    notify      => [
      Service[$synapse::service_name],
      Service[$service_name],
    ],
  }

  file { $service_file:
    ensure => file,
    source => "puppet:///modules/${module_name}/matrix-appservice-irc.service",
  } ~> Exec['systemctl daemon-reload']

  exec { 'systemctl daemon-reload':
    refreshonly => true,
    provider    => shell,
  }

  file { $service_env_file:
    content => @("EOF")
    CONFIG=${config_file}
    REGISTRATION=${registration_file}
    PORT=${listen_port}
    | EOF
  }

  service { $service_name:
    ensure    => running,
    enable    => true,
    require   => [
      File[$service_file],
      File[$service_env_file],
    ],
    subscribe => [
      File[$service_file],
      File[$service_env_file],
      Exec['systemctl daemon-reload']
    ],
  }

}
