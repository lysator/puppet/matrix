class matrix::database::postgres (
  String $user,
  String $db_name,
  Sensitive[String] $db_pass,
) {

  create_resources('Postgresql::Server::Role',
    $db_users, {
      update_password => true,
      login           => true,
    })

  postgresql::server::database { $db_name:
    encoding => 'UTF-8'
    locale   => 'C',
    owner    => $user,
    # before Class['synapse']
  }

}
