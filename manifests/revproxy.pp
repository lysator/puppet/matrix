# https://matrix-org.github.io/synapse/latest/reverse_proxy.html?highlight=nginx#nginx
class matrix::revproxy (
) {

  include ::letsencrypt 
  letsencrypt::cert { $matrix::servername:
    include_self  => true,
    authenticator => 'nginx',
    email         => 'hugo@lysator.liu.se',
    config        => {
      'post-hook' => 'nginx -s reload',
    }
  }

  nginx::resource::server { $matrix::servername:
    *                    => letsencrypt::conf::nginx($matrix::servername),
    ipv6_enable          => true,
    use_default_location => false,
    client_max_body_size => $matrix::max_upload_size,
  }

  $well_known_client = to_json({
    'm.homeserver' => {
      'bare_url' => $matrix::servername,
    },
  })

  $well_known_server = to_json({
    'm.server' => $matrix::servername,
  })


  nginx::resource::location { '~ ^(/_matrix|/_synapse/client)':
    server      => $matrix::servername,
    index_files => [],
    *           => letsencrypt::conf::nginx::location($matrix::servarname),
    proxy              => "http://localhost:${matrix::listen_port}",
    proxy_http_version => '1.1',
    proxy_set_header   => [
      'X-Forwarded-For $remote_addr',
      'X-Forwarded-Proto $scheme',
      'Host $host',
    ],
  }

  nginx::resource::location {
    default:
      server      => $matrix::servername,
      index_files => [],
      add_header  => {
        'Access-Control-Allow-Origin' => '*',
      },
      *           => letsencrypt::conf::nginx::location($matrix::servarname),
      ;
    '/.well-known/matrix/server':
      location_cfg_append => {
        'default_type' => 'application/json',
        'return'       => "200 '${well_known_server}'",
      },
      ;
    '/.well-known/matrix/client':
      location_cfg_append => {
        'default_type' => 'application/json',
        'return'       => "200 '${well_known_client}'",
      },
      ;
  }

}
